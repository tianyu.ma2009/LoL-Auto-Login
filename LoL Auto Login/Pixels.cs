﻿using System.Drawing;

namespace LoLAutoLogin
{

    class Pixels
    {

        public static Pixel LaunchButton = new Pixel(new PixelCoord(0.5f, true), new PixelCoord(15), Color.FromArgb(255, 170, 110, 10), Color.FromArgb(255, 210, 140, 30));
        public static Pixel PasswordBox = new Pixel(new PixelCoord(0.192f, true), new PixelCoord(0.48f, true), Color.FromArgb(255, 240, 240, 240), Color.FromArgb(255, 250, 250, 250));

    }

}
